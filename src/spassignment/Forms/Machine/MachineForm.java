package spassignment.Forms.Machine;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MachineForm extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nameTextField;
    private JComboBox comboBox1;
    private JTextField speedTextField;
    private JTextField idTextField;
    private JLabel statusText;

    private static InetAddress host;
    private static Socket socket = null;
    private static final int PORT = 3004;


    Scanner Input = null;
    PrintWriter Output = null;

    public MachineForm() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        try
        {
            host = InetAddress.getLocalHost();
            if(socket == null)
            {

               socket = new Socket(host,PORT);
            }

            Input = new Scanner(socket.getInputStream());
            Output = new PrintWriter(socket.getOutputStream(),true);
            JSONObject outJson = new JSONObject();
            outJson.put("action","CREATECLIENT");
            outJson.put("type","MACHINE");
            outJson.put("name",nameTextField.getText());
            outJson.put("speed",speedTextField.getText());
            outJson.put("id",idTextField.getText());
            outJson.put("machineType",comboBox1.getSelectedItem().toString());
            Output.println(outJson.toJSONString());

            buttonOK.setEnabled(false);


            Thread thread = new Thread(){
                public void run(){
                    do
                    {
                        String response = Input.nextLine();
                        HandleResponse(response);
                    }while(true);
                }
            };

            thread.start();

        }
        catch(UnknownHostException uhEx)
        {
            System.out.println("\nHost ID not found!\n");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }



    private void onCancel() {
// add your code here if necessary
        dispose();
    }




    public void HandleResponse(String response){

        JSONObject actionJson = (JSONObject) JSONValue.parse(response);

        System.out.println(response);
        String action = actionJson.get("action").toString();

        if(action.equals("MESSAGE")) {

            System.out.println(actionJson.get("message").toString());
        }

        else if(action.equals("SETSTATUS")){

            statusText.setText(actionJson.get("status").toString());
        }
    }

    public static void main(String[] args) {
        MachineForm dialog = new MachineForm();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
