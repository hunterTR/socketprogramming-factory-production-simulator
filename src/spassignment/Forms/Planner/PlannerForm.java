package spassignment.Forms.Planner;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by cemkaya on 01/05/16.
 */
public class PlannerForm extends JDialog{
    private JPanel panel1;
    private JComboBox comboBox1;
    private JList machineListView;
    private JButton GETButton;
    private JButton NEWJOBButton;
    private JList jobListView;
    private JButton SEEDETAILSButton;

    private static InetAddress host;
    private static Socket socket = null;
    private static final int PORT = 3004;
    DefaultListModel<String> machineListModel = new DefaultListModel<>();
    DefaultListModel<String> jobListModel = new DefaultListModel<>();

    Scanner Input = null;
    PrintWriter Output = null;

    public PlannerForm() {

        machineListView.setModel(machineListModel);
        jobListView.setModel(jobListModel);

        setContentPane(panel1);
        setModal(true);

        GETButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onGET();
            }
        });

        NEWJOBButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onNewJob();
            }
        });

        SEEDETAILSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSeeDetails();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        panel1.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        ConnectToServer();
    }

    private void onNewJob() {
        NewJobDialog dialog = new NewJobDialog(Output);
        dialog.pack();
        dialog.setVisible(true);

    }


    private void ConnectToServer() {
// add your code here
        try
        {
            host = InetAddress.getLocalHost();
            if(socket == null)
            {

                socket = new Socket(host,PORT);
            }

            Input = new Scanner(socket.getInputStream());
            Output = new PrintWriter(socket.getOutputStream(),true);
            JSONObject outJson = new JSONObject();
            outJson.put("action","CREATECLIENT");
            outJson.put("type","PLANNER");
            Output.println(outJson.toJSONString());

            Thread thread = new Thread(){
                public void run(){
                    do
                    {
                        String response = Input.nextLine();
                        HandleResponse(response);
                    }while(true);
                }
            };

            thread.start();


        }
        catch(UnknownHostException uhEx)
        {
            System.out.println("\nHost ID not found!\n");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void onGET() {
// add your code here if necessary
        JSONObject outJson = new JSONObject();
        outJson.put("action","GETMACHINES");
        outJson.put("type",comboBox1.getSelectedItem().toString());
        Output.println(outJson.toJSONString());

        outJson = new JSONObject();
        outJson.put("action","GETJOBS");
        outJson.put("type",comboBox1.getSelectedItem().toString());
        Output.println(outJson.toJSONString());
    }

    public void onSeeDetails(){

        JSONObject outJson = new JSONObject();
        outJson.put("action","GETMACHINEJOBHISTORY");
        outJson.put("name",machineListView.getSelectedValue().toString());
        Output.println(outJson.toJSONString());
    }

    public void HandleResponse(String response){

        JSONObject actionJson = (JSONObject) JSONValue.parse(response);

        System.out.println(response);
        String action = actionJson.get("action").toString();

        if(action.equals("MESSAGE")) {

        System.out.println(actionJson.get("message").toString());
        }
        else if(action.equals("GETMACHINESRESPONSE"))
        {
            JSONArray datas = (JSONArray) actionJson.get("machinelist");
            DefaultListModel<String> machineListModel = new DefaultListModel<>();
            for (Object obj :
                    datas) {
                JSONObject test = (JSONObject) obj;

                machineListModel.addElement(test.get("name").toString());

        }
            machineListView.setModel(machineListModel);
            System.out.println("GETMACHINESRESPONSE a girdi");
        }

        else if(action.equals("GETJOBSRESPONSE"))
        {
            JSONArray datas = (JSONArray) actionJson.get("joblist");
            DefaultListModel<String> jobListModel = new DefaultListModel<>();
            for (Object obj :
                    datas) {
                JSONObject test = (JSONObject) obj;
              jobListModel.addElement(test.get("name").toString());
            }
            jobListView.setModel(jobListModel);

            System.out.println("GETJOBSRESPONSE a girdi");
        }
        else if(action.equals("GETMACHINEJOBHISTORYRESPONSE"))
        {

            MachineDetailsDialog dialog = new MachineDetailsDialog(actionJson);
            dialog.pack();
            dialog.setVisible(true);


            System.out.println("GETMACHINEJOBHISTORYRESPONSE a girdi");
        }
        else if(action.equals("REFRESH"))
        {
            onGET();
        }




    }
    private void onCancel() {
// add your code here if necessary
        dispose();
    }
    public static void main(String[] args) {
        PlannerForm dialog = new PlannerForm();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
