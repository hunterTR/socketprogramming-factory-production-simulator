package spassignment.Forms.Planner;

import org.json.simple.JSONObject;

import javax.swing.*;
import java.awt.event.*;
import java.io.PrintWriter;
import java.util.Random;

public class NewJobDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField durationField;
    private JComboBox comboBox1;
    private JTextField nameField;
    private PrintWriter Output;
    private Random generator;

    public NewJobDialog(PrintWriter output) {
        Output = output;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        generator = new Random();
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        JSONObject outJson = new JSONObject();
        outJson.put("action","NEWJOB");
        outJson.put("type",comboBox1.getSelectedItem().toString());
        outJson.put("name",nameField.getText());
        outJson.put("duration",durationField.getText());
        outJson.put("id",generator.nextInt(1000));
        Output.println(outJson.toJSONString());
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
//        NewJobDialog dialog = new NewJobDialog();
//        dialog.pack();
//        dialog.setVisible(true);
//        System.exit(0);
    }
}
