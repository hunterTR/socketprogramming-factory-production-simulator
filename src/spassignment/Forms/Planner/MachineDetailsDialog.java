package spassignment.Forms.Planner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.swing.*;
import java.awt.event.*;

public class MachineDetailsDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList jobHistoryList;
    private JLabel IDField;
    private JLabel jobName;
    private JLabel machineName;
    private JLabel typeLabel;
    private JButton buttonCancel;

    public MachineDetailsDialog(JSONObject actionJson) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        JSONArray datas = (JSONArray) actionJson.get("joblist");

        IDField.setText( "ID: " + actionJson.get("id").toString());
        this.machineName.setText( actionJson.get("machineName").toString());
        this.jobName.setText("Current Job: " +  actionJson.get("currentJobName").toString());
        this.jobName.setText("Current Job: " +  actionJson.get("currentJobName").toString());
        this.typeLabel.setText("TYPE: " + actionJson.get("type").toString());
        DefaultListModel<String> jobListModel = new DefaultListModel<>();
        for (Object obj :
                datas) {
            JSONObject test = (JSONObject) obj;
            jobListModel.addElement(test.get("name").toString());
        }

        jobHistoryList.setModel(jobListModel);
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });



// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {

    }
}
