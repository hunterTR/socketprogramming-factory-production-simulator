package spassignment.Forms.Planner;

/**
 * Created by cemkaya on 02/05/16.
 */
// in a background thread.
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.swing.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class PlannerBackgroundService extends SwingWorker< Long, Object >
{
    private JList machineListView;
    private JList jobListView;

    Scanner Input = null;
    PrintWriter Output = null;

    // constructor
    public PlannerBackgroundService( JList machineListView,JList jobListView)
    {
        this.machineListView = machineListView;
        this.jobListView = jobListView;
    } // end BackgroundCalculator constructor

    // long-running code to be run in a worker thread
    public Long doInBackground()
    {

        return (long)1;
    } // end method doInBackground

    // code to run on the event dispatch thread when doInBackground returns
    protected void done()
    {
    } // end method done

    // recursive method fibonacci; calculates nth Fibonacci number

} // end class BackgroundCalculator

