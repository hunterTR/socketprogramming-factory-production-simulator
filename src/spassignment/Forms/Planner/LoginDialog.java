package spassignment.Forms.Planner;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class LoginDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textField1;
    private JTextField textField2;

    private static InetAddress host;
    private static Socket socket = null;
    private static final int PORT = 3004;
    DefaultListModel<String> machineListModel = new DefaultListModel<>();
    DefaultListModel<String> jobListModel = new DefaultListModel<>();

    Scanner Input = null;
    PrintWriter Output = null;

    public LoginDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here

        ConnectToServer();

    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    private void ConnectToServer() {
// add your code here
        try
        {
            host = InetAddress.getLocalHost();
            if(socket == null)
            {

                socket = new Socket(host,PORT);
            }

            Input = new Scanner(socket.getInputStream());
            Output = new PrintWriter(socket.getOutputStream(),true);
            JSONObject outJson = new JSONObject();
            outJson.put("action","LOGINREQUEST");
            outJson.put("username",textField1.getText());
            outJson.put("password",textField2.getText());

            Output.println(outJson.toJSONString());

            String response = Input.nextLine();

            JSONObject actionJson = (JSONObject) JSONValue.parse(response);

            System.out.println(response);
            String action = actionJson.get("action").toString();

            if(action.equals("MESSAGE")) {

                System.out.println(actionJson.get("message").toString());
            }
            else if(action.equals("LOGINRESPONSE")){
                boolean result = (boolean)actionJson.get("result");

                if(result){
                    PlannerForm dialog = new PlannerForm();
                    dialog.pack();
                    dialog.setVisible(true);
                    dispose();
                }else
                {
                    dispose();
                }

            }



        }
        catch(UnknownHostException uhEx)
        {
            System.out.println("\nHost ID not found!\n");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        LoginDialog dialog = new LoginDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
