/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spassignment;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import spassignment.Models.MachineInformation;
import spassignment.Models.User;
import spassignment.Threads.MachineListener;
import spassignment.Threads.PlannerListener;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author cem.kaya
 */
public class SPAssignmentServer {

    public static final int LISTENING_PORT = 3004;
    public static CopyOnWriteArrayList<MachineListener> machineListeners = new CopyOnWriteArrayList<MachineListener>();
    public static CopyOnWriteArrayList<User> userList = new CopyOnWriteArrayList<User>();
    public static ArrayBlockingQueue<PlannerListener> plannerListeners = new ArrayBlockingQueue<PlannerListener>(20);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        userList.add(new User("Cem","Cem"));
        userList.add(new User("Cem2","Cem2"));
        ServerSocket serverSocket = null;
        try {
           serverSocket = new ServerSocket(LISTENING_PORT);
           System.out.println("Factory simulation server started on port " + LISTENING_PORT);
        } catch (IOException se) {
           System.err.println("Can not start listening on port " + LISTENING_PORT);
           se.printStackTrace();
           System.exit(-1);
        }


        Scanner input;
        PrintWriter output;

        do
        {
            //Wait for client...

            Socket client = null;
            try {
                client = serverSocket.accept();
                System.out.println("\nNew Connection.\n");
                input = new Scanner(client.getInputStream());
                output = new PrintWriter(
                        client.getOutputStream(),true);

                String received = input.nextLine();
                JSONObject actionJson = (JSONObject) JSONValue.parse(received);

                String action = actionJson.get("action").toString();

                if(action.equals("CREATECLIENT"))
                {
                    String type = actionJson.get("type").toString();
                    if(type.equals("MACHINE"))
                    {
                        System.out.println("MACHINE CLIENT DETECTED");
                        MachineInformation mInformation = new MachineInformation(actionJson.get("name").toString(),
                                Integer.parseInt(actionJson.get("id").toString()),actionJson.get("machineType").toString(),
                                Integer.parseInt(actionJson.get("speed").toString()));
                        MachineListener machine = new MachineListener(client,mInformation);
                        machineListeners.add(machine);
                        machine.start();

                        for (PlannerListener planner:
                             plannerListeners) {
                            JSONObject outputJson = new JSONObject();
                            outputJson.put("action", "REFRESH");
                            planner.Output.println(outputJson.toJSONString());
                        }
                    }
                    else if(type.equals("PLANNER"))
                    {
                        System.out.println("PLANNER CLIENT DETECTED");
                        PlannerListener planner = new PlannerListener(client, machineListeners);
                        plannerListeners.add(planner);
                        planner.start();
                    }
                }
                else if(action.equals("LOGINREQUEST"))
                {

                    String username = actionJson.get("username").toString();
                    String password = actionJson.get("password").toString();

                    boolean found = false;
                    for (User user:
                            userList) {
                        if(user.UserName.equals(username) && user.Password.equals(password) && !user.isLoggedIn())
                        {
                            JSONObject outputJson = new JSONObject();
                            outputJson.put("action", "LOGINRESPONSE");
                            outputJson.put("result", true);

                            output.println(outputJson.toJSONString());
                            found = true;
                            user.setLoggedIn(true);
                        }
                    }

                    if(!found)
                    {
                        JSONObject outputJson = new JSONObject();
                        outputJson.put("action", "LOGINRESPONSE");
                        outputJson.put("result", false);
                        output.println(outputJson.toJSONString());
                    }
                }

                // Open client according to data..

            } catch (IOException e) {
                e.printStackTrace();
            }

        }while (true);




        
    }





    
}
