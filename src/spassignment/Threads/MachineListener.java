/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spassignment.Threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import spassignment.Models.Job;
import spassignment.Models.MachineInformation;

/**
 *
 * @author cem.kaya
 */
public class MachineListener extends Thread {

    public ArrayBlockingQueue<Job> JobQueue = new ArrayBlockingQueue<Job>(5);
    public List<Job> JobHistory;
    public Socket Socket;
    public BufferedReader Input;
    public PrintWriter Output;
    public MachineInformation Information;
    public boolean busy;
    public boolean working;
    public Job currentJob = null;
    public MachineListener(Socket socket, MachineInformation information) {
        this.JobHistory = new ArrayList<Job>();
        this.Socket = socket;
        this.Information = information;
        this.busy = false;
        try {
            Input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            Output = new PrintWriter(socket.getOutputStream(), true);

            JSONObject outputJson = new JSONObject();
            outputJson.put("action", "MESSAGE");
            outputJson.put("message", "Successfully connected");
            Output.println(outputJson.toJSONString());
        } catch (IOException ex) {
            Logger.getLogger(PlannerListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        // Process all messages from server, according to the protocol.
        while (true) {
            String line;
            try {
                line = Input.readLine();

                JSONObject inputJson = (JSONObject) JSONValue.parseWithException(line);

                if (inputJson.get("action").equals("GETINFORMATION")) {
                    JSONObject outputJson = new JSONObject();
                    outputJson.put("action", "MACHINEINFORMATION");
                    outputJson.put("id", Information.Id);
                    outputJson.put("name", Information.Name);
                    outputJson.put("type", Information.Type);
                    outputJson.put("busy", busy);
                    Output.println(outputJson.toJSONString());
                } else if (inputJson.get("action").equals("SETINFORMATION")) {
                    Information.Id = (int) inputJson.get("id");
                    Information.Name = (String) inputJson.get("name");
                    Information.Type = (String) inputJson.get("type");
                    
                    JSONObject outputJson = new JSONObject();
                    outputJson.put("action", "MESSAGE");
                    outputJson.put("message", "Machine Infromation has been set.");
                    Output.println(outputJson.toJSONString());
                    
                }
                else if (inputJson.get("action").equals("MESSAGE")) {
                    System.out.println(inputJson.get("message"));
                }
            } catch (IOException ex) {
                Logger.getLogger(MachineListener.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(MachineListener.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }


    public void workWork(final PrintWriter plannerOutput)
    {
        System.out.println("Jobque size " + JobQueue.size() );
        System.out.println("Job History size " + JobHistory.size() );
        Thread thread = new Thread(){
            public void run(){
                do{
                    System.out.println("threade girdi");
                    if(JobQueue.size() > 0)
                    {
                        try {
                            currentJob = JobQueue.take();
                            Thread.sleep(currentJob.Duration/Information.Speed);
                            JSONObject outJson = new JSONObject();
                            outJson.put("action","REFRESH");
                            plannerOutput.println(outJson.toJSONString());
                            JobHistory.add(currentJob);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        busy = false;
                        working = false;
                        JSONObject outJson = new JSONObject();
                        outJson.put("action","SETSTATUS");
                        outJson.put("status","EMPTY");
                        currentJob = null;
                        Output.println(outJson.toJSONString());
                    }
                }while(working);
            }
        };

        thread.start();

    }

}
