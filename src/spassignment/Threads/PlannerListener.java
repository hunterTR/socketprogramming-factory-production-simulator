/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spassignment.Threads;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import spassignment.Models.Job;
import spassignment.Models.User;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cem.kaya
 */
public class PlannerListener extends Thread {
    public CopyOnWriteArrayList<MachineListener> machineListeners;
    public static CopyOnWriteArrayList<User> userList = new CopyOnWriteArrayList<User>();
    Socket Socket;
    public Scanner Input;
    public PrintWriter Output;
    
    public PlannerListener(Socket socket, CopyOnWriteArrayList<MachineListener> machineList){
        this.Socket = socket;
        this.machineListeners = machineList;
        try {
            Input = new Scanner(socket.getInputStream());
            Output = new PrintWriter(
                    Socket.getOutputStream(),true);

            JSONObject outputJson = new JSONObject();
            outputJson.put("action", "MESSAGE");
            outputJson.put("message", "Successfully connected");
            Output.println(outputJson.toJSONString());
        } catch (IOException ex) {
            Logger.getLogger(PlannerListener.class.getName()).log(Level.SEVERE, null, ex);
        }
               
    }
    
    public void run() {
        // The thread is only started after everyone connects.
        //   Output.println("MESSAGE All players connected");


        // Repeatedly get commands from the client and process them.
        while (true) {
            String received = Input.nextLine();
            System.out.println(received);
            JSONObject actionJson = (JSONObject) JSONValue.parse(received);

            String action = actionJson.get("action").toString();
            if (action.equals("NEWJOB")) {
                JSONObject outputJson = new JSONObject();
                if(machineListeners.size() > 0)
                {
                    if(AssignJob(actionJson))
                    {
                        outputJson.put("action", "MESSAGE");
                        outputJson.put("message", "Job has been assigned");
                    }else
                    {
                        outputJson.put("action", "MESSAGE");
                        outputJson.put("message", "There are no machine that can handle this job.");
                    }
                }else
                {
                    outputJson.put("action", "MESSAGE");
                    outputJson.put("message", "There are no active machines.");
                }

                Output.println(outputJson.toJSONString());
                outputJson = new JSONObject();
                outputJson.put("action", "REFRESH");
                Output.println(outputJson.toJSONString());

            } else if (action.equals("GETMACHINES")) {
                JSONObject outputJson = new JSONObject();
                JSONArray list = new JSONArray();
                for (MachineListener machine :
                        machineListeners) {

                    if(machine.Information.Type.equals(actionJson.get("type").toString()))
                    {
                        JSONObject machineJson = new JSONObject();
                        machineJson.put("name", machine.Information.Name);
                        machineJson.put("type", machine.Information.Type);
                        machineJson.put("id", machine.Information.Id);
                        list.add(machineJson);
                    }
                }

                outputJson.put("action", "GETMACHINESRESPONSE");
                outputJson.put("machinelist", list);
                Output.println(outputJson.toJSONString());
            }
            else if(action.equals("GETJOBS"))
            {
                JSONObject outputJson = new JSONObject();
                JSONArray list = new JSONArray();

                for (MachineListener machine :
                        machineListeners) {

                    if(machine.JobQueue.size() > 0 && machine.Information.Type.equals(actionJson.get("type").toString()))
                    {
                        for (Job job :
                                machine.JobQueue) {

                            JSONObject jobJson = new JSONObject();
                            jobJson.put("name", job.Name);
                            jobJson.put("id", job.Id);
                            jobJson.put("machineName",machine.Information.Name);
                            jobJson.put("machineId",machine.Information.Id);
                            list.add(jobJson);
                        }
                    }

                }
                outputJson.put("action", "GETJOBSRESPONSE");
                outputJson.put("joblist", list);
                Output.println(outputJson.toJSONString());
            }
            else if(action.equals("GETMACHINEJOBHISTORY"))
            {
                JSONObject outputJson = new JSONObject();
                JSONArray list = new JSONArray();

                for (MachineListener machine :
                        machineListeners) {

                    if(machine.Information.Name.equals(actionJson.get("name").toString()))
                    {
                        for (Job job :
                                machine.JobHistory) {

                            JSONObject jobJson = new JSONObject();
                            jobJson.put("machineId",machine.Information.Id);
                            jobJson.put("name", job.Name);
                            jobJson.put("id", job.Id);
                            jobJson.put("machineName",machine.Information.Name);

                            list.add(jobJson);
                        }

                        if(machine.currentJob != null)
                        {
                            outputJson.put("currentJobName",machine.currentJob.Name);
                        }
                        else
                        {
                            outputJson.put("currentJobName","NONE");
                        }
                        outputJson.put("machineName",machine.Information.Name);
                        outputJson.put("type",machine.Information.Type);
                        outputJson.put("id",machine.Information.Id);
                    }
                }
                outputJson.put("action", "GETMACHINEJOBHISTORYRESPONSE");
                outputJson.put("joblist", list);

                Output.println(outputJson.toJSONString());


            }




        }
    }

    public boolean AssignJob(JSONObject actionJson){
        String type = actionJson.get("type").toString();
        int Duration = Integer.parseInt(actionJson.get("duration").toString());
        int length = 100;
        int index = 0;
        int selectedindex = 0;
        boolean result = false;
        for (MachineListener machine: machineListeners) {

            if(machine.Information.Type.equals(type))
            {
                if(machine.JobQueue.size() < length)
                {
                    length = machine.JobQueue.size();
                    selectedindex = index;
                    result = true;
                }

            }

            index++;
        }

        if(result)
        {
            machineListeners.get(selectedindex).JobQueue.add(new Job(Integer.parseInt(actionJson.get("id").toString()),actionJson.get("name").toString(),Duration));
            if(!machineListeners.get(selectedindex).working) {
                JSONObject outJson = new JSONObject();
                outJson.put("action","SETSTATUS");
                outJson.put("status","BUSY");
                machineListeners.get(selectedindex).Output.println(outJson.toJSONString());
                machineListeners.get(selectedindex).working = true;
                machineListeners.get(selectedindex).workWork(Output);
            }
        }

        return result;

    }
        
}
