package spassignment.Models;

/**
 * Created by cemkaya on 01/05/16.
 */
public class Job {

    public int Id;
    public String Name;
    public String Type;
    public int Duration;
    public Job(int id,String name,int duration) {
        Id = id;
        Name = name;
        Duration = duration;
    }

}
