package spassignment.Models;

/**
 * Created by cemkaya on 04/05/16.
 */
public class User {
    public String UserName;
    public String Password;
    public boolean LoggedIn;

    public User(String username,String password)
    {
        UserName = username;
        Password = password;
        LoggedIn = false;
    }

    public boolean isLoggedIn() {
        return LoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        LoggedIn = loggedIn;
    }
}
