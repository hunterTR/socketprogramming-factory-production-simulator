/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spassignment.Models;

/**
 *
 * @author cem.kaya
 */
public class MachineInformation {
    public String Name;
    public int Id;
    public int Speed;
    public String Type;
    
    public MachineInformation(String machineName , int id, String machineType,int speed){
        this.Name = machineName;
        this.Id = id;
        this.Type = machineType;
        this.Speed = speed;
    }
    
    public String ToString(){
        return Id + ";" + Name + ";" + Type;
    }
}
